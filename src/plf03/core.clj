(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [xs] (filter neg? xs))
        g (fn [xs] (filter neg-int? xs))
        h (fn [xs] (filter  pos? xs))
        z (comp f g h)]
    (z #{-123 10 233})))



(defn función-comp-2
  []
  (let [f (fn [xs] (map pos? xs))
        g (fn [xs] (filter even? xs))
        h (fn [xs] (filter inc xs))
        z (comp f g h)]
    (z [12 -13 0 19 0])))



(defn función-comp-3
  []
  (let [f (fn [xs] (filter int? xs))
        g (fn [xs] (map even? xs))
        h (fn [xs] (filter decimal? xs))
        z (comp f g h)]
    (z '( 12 30 1.1))))



(defn función-comp-4
  []
  (let [f (fn [xs] (filterv decimal? xs))
        g (fn [xs] (filter int? xs))
        h (fn [xs] (map integer? xs))
        z (comp f g h)]
    (z #{ 10.1 1 2 3 })))


(defn función-comp-5
  []
  (let [f (fn [xs] (filter integer? xs))
        g (fn [xs] (filter int? xs))
        h (fn [xs] (filter decimal? xs))
        z (comp f g h)]
    (z #{ 1 2 4 6})))



(defn función-comp-6
  []
  (let [f (fn [xs] ( string? xs))
        g (fn [xs] ( int? xs))
        h (fn [xs] ( integer? xs))
        z (comp f g h)]
    (z #{["123"] ["24"] ["234"]})))



(defn función-comp-7
  []
  (let [f (fn [xs] ( filter integer?  xs))
        g (fn [xs] ( filter int? xs))
        h (fn [xs] ( filter zero? xs))
        z (comp f g h)]
    (z #{ 0 1 2 3})))



(defn función-comp-8
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (vector? xs))
        h (fn [xs] (filter decimal? xs))
        z (comp f g h)]
    (z '('() [] 1.0))))



(defn función-comp-9
  []
  (let [f (fn [xs] (float? xs))
        g (fn [xs] (rational? xs))
        h (fn [xs] (string? xs))
        z (comp f g h)]
    (z #{10.1 1/2 "xd"})))




(defn función-comp-10
  []
  (let [f (fn [xs] (some? xs))
        g (fn [xs] (symbol? xs))
        h (fn [xs] ( string? xs))
        z (comp f g h)]
    (z #{1 2 4 6})))


(defn función-comp-11
  []
  (let [f (fn [xs] (set? xs))
        g (fn [xs] (filter pos? xs))
        h (fn [xs] (filter neg? xs))
        z (comp f g h)]
    (z #{#{1 2} 3  -1})))



(defn función-comp-12
  []
  (let [f (fn [xs] (filter zero?  xs))
        g (fn [xs] (filter rational? xs))
        h (fn [xs] (filter nil? xs))
        z (comp f g h)]
    (z [0 1 2 3])))




(defn función-comp-13
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (list? xs))
        h (fn [xs] (filter zero? xs))
        z (comp f g h)]
    (z '('() [] 0.0))))




(defn función-comp-14
  []
  (let [f (fn [xs] (float? xs))
        g (fn [xs] (int? xs))
        h (fn [xs] (char? xs))
        z (comp f g h)]
    (z #{10.1 1/3 "TEST"})))


(defn función-comp-15
  []
  (let [f (fn [xs] (set? xs))
        g (fn [xs] (string? xs))
        h (fn [xs] (symbol? xs))
        z (comp f g h)]
    (z '(#{1 2} "hi" 1))))

(defn función-comp-16
  []
  (let [f (fn [xs] (char? xs))
        g (fn [xs] (filter pos? xs))
        h (fn [xs] (filter neg? xs))
        z (comp f g h)]
    (z '( 1 2 3 4))))

(defn función-comp-17
  []
  (let [f (fn [xs] (filter zero?  xs))
        g (fn [xs] (filter nil? xs))
        h (fn [xs] (filter even? xs))
        z (comp f g h)]
    (z [0 1 2 3])))



(defn función-comp-18
  []
  (let [f (fn [xs] (int? xs))
        g (fn [xs] (decimal? xs))
        h (fn [xs] (integer? xs))
        z (comp f g h)]
    (z '([] 0.0 1 ))))



(defn función-comp-19
  []
  (let [f (fn [xs] (nat-int? xs))
        g (fn [xs] (seq? xs))
        h (fn [xs] (string? xs))
        z (comp f g h)]
    (z #{0.0 -1 ""})))



(defn función-comp-20
  []
  (let [f (fn [xs] (rational? xs))
        g (fn [xs] (symbol? xs))
        h (fn [xs] (filter xs))
        z (comp f g h)]
    (z '(#{} 1 2 3))))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)


(defn función-complement-1
  []
  (let [f (fn [xs] (filter neg? xs))
        z (complement f )]
    (z #{123})))

(defn función-complement-2
  []
  (let [f (fn [xs] (filter pos? xs))
        z (complement f )]
    (z [0])))


(defn función-complement-3
  []
  (let [f (fn [xs] (filter int? xs))
        z (complement f)]
    (z '(12))))



(defn función-complement-4
  []
  (let [
        f (fn [xs] (map integer? xs))
        z (complement f)]
    (z #{10.1 })))

(defn función-complement-5
  []
  (let [f (fn [xs] (filter pos? xs))
        z (complement f )]
    (z #{1 2 4 6})))



(defn función-complement-6
  []
  (let [f (fn [xs] (string? xs))
        z (complement f )]
    (z '("tres"))))


(defn función-complement-7
  []
  (let [f (fn [xs] (filter char? xs))
        z (complement f)]
    (z [1223])))


(defn función-complement-8
  []
  (let [f (fn [xs] (rational? xs))
        z (complement f)]
    (z '(1))))


(defn función-complement-9
  []
  (let [f (fn [xs] (ratio? xs))
        z (complement f)]
    (z [1223])))


(defn función-complement-10
  []
  (let [f (fn [xs] (list? xs))
        z (complement f)]
    (z '(1 2 3 ))))


(defn función-complement-11
  []
  (let [f (fn [xs] (vector? xs))
        z (complement f)]
    (z [ 2 1 ])))


(defn función-complement-12
  []
  (let [f (fn [xs] (map? xs))
        z (complement f)]
    (z #{ 1 2 3 4 5})))


(defn función-complement-13
  []
  (let [f (fn [xs] (pos-int? xs))
        z (complement f)]
    (z [-10])))


(defn función-complement-14
  []
  (let [f (fn [xs] (some? xs))
        z (complement f)]
    (z #{0 1 2 3 4})))

(defn función-complement-15
  []
  (let [f (fn [xs] (number? xs))
        z (complement f)]
    (z [230])))


(defn función-complement-16
  []
  (let [f (fn [xs] (string? xs))
        z (complement f)]
    (z #{"test"})))


(defn función-complement-17
  []
  (let [f (fn [xs] (coll? xs))
        z (complement f)]
    (z [20 12 34])))

(defn función-complement-18
  []
  (let [f (fn [xs] (integer? xs))
        z (complement f)]
    (z '(234))))

(defn función-complement-19
  []
  (let [f (fn [xs] (ident? xs))
        z (complement f)]
    (z [0 1 2 1])))

(defn función-complement-20
  []
  (let [f (fn [xs] (double? xs))
        z (complement f)]
    (z '(2.1))))

(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)



(defn función-constantly-1
  []
  (let [xs #{ :a :b :c}
        z (constantly xs)]
    (z #{ 1 2 3})))

(defn función-constantly-2
  []
  (let [xs [:a]
        z (constantly xs)]
    (z [2])))

(defn función-constantly-3
  []
  (let [x 1
        z (constantly x)]
    (z -1)))


(defn función-constantly-4
  []
  (let [x false
        z (constantly x)]
    (z true)))


(defn función-constantly-5
  []
  (let [x {"infernus" "caos"}
        z (constantly x)]
    (z {"caos" "infernus"})))


(defn función-constantly-6
  []
  (let [x #{ "testConjunto" "ConjuntoTest"}
        z (constantly x)]
    (z #{"ConjuntoTest" "testConjuto"})))

(defn función-constantly-7
  []
  (let [x #{0 nil 1}
        z (constantly x)]
    (z #{nil 0 -1})))


(defn función-constantly-8
  []
  (let [x [nil]
           z (constantly x)]
    (z [0])))

(defn función-constantly-9
  []
  (let [x '(#{a b} [nil])
        z (constantly x)]
    (z '('(a b) [0]))))

(defn función-constantly-10
  []
  (let [x [true false]
           z (constantly x)]
    (z '( "no regresare nada de aca"))))

(defn función-constantly-11
  []
  (let [x [true false]
        z (constantly x)]
    (z [false true ])))

(defn función-constantly-12
  []
  (let [x ['A']
        z (constantly x)]
    (z ["string vs char"])))

(defn función-constantly-13
  []
  (let [x [#{1 2} [:1 :2]]
        z (constantly x)]
    (z [[0 1 2 3]])))

(defn función-constantly-14
  []
  (let [x 0
        z (constantly x)]
    (z [0])))

(defn función-constantly-15
  []
  (let [x [ 1 2 3 4]
        z (constantly x)]
    (z [x [0 2 3 4]])))

(defn función-constantly-16
  []
  (let [x '(nil)
           z (constantly x)]
    (z [x [nil]])))

(defn función-constantly-17
  []
  (let [x 0
        z (constantly x)]
    (z  1)))

(defn función-constantly-18
  []
  (let [x "EL DIABLO ANDA SUELTO"
        z (constantly x)]
    (z  ["DIOS ESTA AQUI"])))

(defn función-constantly-19
  []
  (let [x [[] '() #{}]
        z (constantly x)]
    (z  [1])))

(defn función-constantly-20
  []
  (let [x [ :a 1 :b 2]
        z (constantly x)]
    (z  [nil])))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)



(defn función-every-pred-1
  []
  (let [f (fn [x] int? x)
        z (every-pred f )]
    (z 123)))

(defn función-every-pred-2
  []
  (let [f (fn [x] neg? x)
        z (every-pred f )]
    (z -0.1 )))

(defn función-every-pred-3
  []
  (let [f (fn [x] boolean? x)
        z (every-pred f )]
    (z true)))

(defn función-every-pred-4
  []
  (let [f (fn [x] boolean? x)
        g(fn[x] number? x)
        z (every-pred f g)]
    (z "a")))

(defn función-every-pred-5
  []
  (let [f (fn [x] neg? x)
        g (fn [x] number? x)
        z (every-pred f g)]
    (z  5)))

(defn función-every-pred-6
  []
  (let [f (fn [x] pos? x)
        g (fn [x] odd? x)
        z (every-pred f g)]
    (z  100)))

(defn función-every-pred-7
  []
  (let [f (fn [x] integer? x)
        g (fn [x] odd? x)
        z (every-pred f g)]
    (z  200)))

(defn función-every-pred-8
  []
  (let [f (fn [x] nat-int? x)
        g (fn [x] int? x)
        z (every-pred f g)]
    (z  110)))

(defn función-every-pred-9
  []
  (let [f (fn [x] nat-int? x)
        g (fn [x] int? x)
        z (every-pred f g)]
    (z  10)))

(defn función-every-pred-10
  []
  (let [f (fn [x] float? x)
        g (fn [x] double? x)
        z (every-pred f g)]
    (z  10.10 )))


(defn función-every-pred-11
  []
  (let [f (fn [x] coll? x)
        g (fn [x] indexed? x)
        z (every-pred f g)]
    (z  [ 1 2 3 4])))

(defn función-every-pred-12
  []
  (let [f (fn [x] list? x)
        g (fn [x] map? x)
        z (every-pred f g)]
    (z  '(1 2 3 4) #{ 0 9 8 7})))


(defn función-every-pred-13
  []
  (let [f (fn [x] ratio? x)
        g (fn [x] rational? x)
        z (every-pred f g)]
    (z  145 )))

(defn función-every-pred-14
  []
  (let [f (fn [x] seq? x)
        g (fn [x] seqable? x)
        z (every-pred f g)]
    (z  245)))

(defn función-every-pred-15
  []
  (let [f (fn [x] float? x)
        g (fn [x] decimal? x)
        z (every-pred f g)]
    (z  2.1)))


(defn función-every-pred-16
  []
  (let [f (fn [x] some? x)
        g (fn [x] pos? x)
        z (every-pred f g)]
    (z  0.1)))

(defn función-every-pred-17
  []
  (let [f (fn [x] double? x)
        g (fn [x] neg? x)
        z (every-pred f g)]
    (z  -0.1)))

(defn función-every-pred-18
  []
  (let [f (fn [x] keyword? x)
        g (fn [x] list? x)
        z (every-pred f g)]
    (z  [:a 1 :b 2])))

(defn función-every-pred-19
  []
  (let [f (fn [x] set? x)
        g (fn [x] some? x)
        z (every-pred f g)]
    (z  100)))

(defn función-every-pred-20
  []
  (let [f (fn [x] float? x)
        g (fn [x] integer? x)
        z (every-pred f g)]
    (z  1.1)))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)


(defn función-fnil-1
  []
  (let [f (fn [x y] (vector? [x y]))
        z (fnil f 10)]
    (z nil 1)))


(defn función-fnil-2
  []
  (let [f (fn [x y ] (vector? [x y]))
        g(fn [z] (int? z))
        z (fnil f 0 g -1)]
    (z nil 1 )))


(defn función-fnil-3
  []
  (let [f (fn [x y] (boolean? [x y]))
        z (fnil f 0 )]
    (z nil 100)))

(defn función-fnil-4
  []
  (let [f (fn [x y] (map? [x y]))
        z (fnil f 0)]
    (z nil 100)))


(defn función-fnil-5
  []
  (let [f (fn [x y] (pos-int? [x y]))
        g (fn [z] (number? z))
        z (fnil f 100 g 10)]
    (z nil 90)))

(defn función-fnil-6
  []
  (let [f (fn [x y] (list? [x y]))
        z (fnil f 0)]
    (z nil '())))

(defn función-fnil-7
  []
  (let [f (fn [x y] (> x y))
        z (fnil f 10)]
    (z nil 9)))

(defn función-fnil-8
  []
  (let [f (fn [x y] (< x y))
        z (fnil f 100)]
    (z nil 9)))

(defn función-fnil-9
  []
  (let [f (fn [x y] ( * x y))
        z (fnil f 0)]
    (z nil 0)))


(defn función-fnil-10
  []
  (let [f (fn [x y] (>= x y))
        z (fnil f 10)]
    (z nil 10)))

(defn función-fnil-11
  []
  (let [f (fn [x y] (<= x y))
        z (fnil f 100)]
    (z nil 1)))

(defn función-fnil-12
  []
  (let [f (fn [x y] (/ x y))
        z (fnil f 1)]
    (z nil 1)))

(defn función-fnil-13
  []
  (let [f (fn [x y] (first[ x y]))
        z (fnil f 1000)]
    (z nil 10)))

(defn función-fnil-14
  []
  (let [f (fn [x y] (last [x y]))
        z (fnil f "test")]
    (z nil "holi")))

(defn función-fnil-15
  []
  (let [f (fn [x y] (count [x y]))
        z (fnil f "je je ")]
    (z nil " XD")))

(defn función-fnil-16
  []
  (let [f (fn [x y] (counted? [x y]))
        z (fnil f "je")]
    (z nil "test")))

(defn función-fnil-17
  []
  (let [f (fn [x y] (string? [x y]))
        z (fnil f "nada es casualidad")]
    (z nil "test")))

(defn función-fnil-18
  []
  (let [f (fn [x y] (take 3 [x y]))
        z (fnil f "lalalalala1234")]
    (z nil "test01")))

(defn función-fnil-19
  []
  (let [f (fn [x y] (map? [x y]))
        z (fnil f { 1 2 3 4})]
    (z nil { 5 6 7 8 })))

(defn función-fnil-20
  []
  (let [f (fn [x y] (str [x y]))
        z (fnil f "string1")]
    (z nil "string2")))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)


(defn función-juxt-1
  []
  (let [f (fn [x] (last x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z "LOQUENDO LOQUENDO")))

(defn función-juxt-2
  []
  (let [f (fn [x] (first x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z "LOQUENDO LOQUENDO")))


(defn función-juxt-3
  []
  (let [f (fn [x] (boolean? x))
        g (fn [x] (integer? x))
        z (juxt f g)]
    (z 1)))

(defn función-juxt-4
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (last x))
        z (juxt f g)]
    (z "test de string")))

(defn función-juxt-5
  []
  (let [f (fn [x] (char? x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z "3")))

(defn función-juxt-6
  []
  (let [f (fn [x] (count x))
        g (fn [x] (counted? x))
        z (juxt f g)]
    (z "EL DIABLO ANDA SUELTO")))

(defn función-juxt-7
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z ["VA PISANDO EL MISMO PAVIMENTO" "X2"])))

(defn función-juxt-8
  []
  (let [f (fn [x] (map? x))
        g (fn [x] (first x))
        z (juxt f g)]
    (z { "my" "name" "is" "slim shady"})))

(defn función-juxt-9
  []
  (let [f (fn [x] (first x))
        g (fn [x] (last x))
        z (juxt f g)]
    (z #{"my" "name" "is" "slim shady"})))

(defn función-juxt-10
  []
  (let [f (fn [x] (str x))
        g (fn [x] (last x))
        z (juxt f g)]
    (z "TEST0102")))

(defn función-juxt-11
  []
  (let [f (fn [x] (list? x))
        g (fn [x] (str x))
        z (juxt f g)]
    (z '(1 2 "134"))))

(defn función-juxt-12
  []
  (let [f (fn [x] (map? x))
        g (fn [x] (associative? x))
        z (juxt f g)]
    (z {:a 1 :b 2 })))

(defn función-juxt-13
  []
  (let [f (fn [x] (boolean? x))
        g (fn [x] (associative? x))
        z (juxt f g)]
    (z {:a true :b false})))


(defn función-juxt-14
  []
  (let [f (fn [x] (decimal? x))
        g (fn [x] (neg? x))
        z (juxt f g)]
    (z -10.0)))

(defn función-juxt-15
  []
  (let [f (fn [x] (double? x))
        g (fn [x] (pos? x))
        z (juxt f g)]
    (z 10.0)))

(defn función-juxt-16
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (nil? x))
        z (juxt f g)]
    (z [])))

(defn función-juxt-17
  []
  (let [f (fn [x] (map? x))
        g (fn [x] (empty? x))
        z (juxt f g)]
    (z [])))

(defn función-juxt-18
  []
  (let [f (fn [x] (nil? x))
        g (fn [x] (empty? x))
        z (juxt f g)]
    (z [ nil []])))


(defn función-juxt-19
  []
  (let [f (fn [x] (integer? x))
        g (fn [x] (pos? x))
        z (juxt f g)]
    (z 0)))


(defn función-juxt-20
  []
  (let [f (fn [x] (keyword? x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z {:a :b :c :d} )))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)


(defn función-partial-1
  []
  (let [f (fn [xs ys] (group-by xs ys))
        z (partial f set)]
  (z ["aris" "iris" "aria" "ira" "ir" "xd"])))

(defn función-partial-2
  []
  (let [f (fn [xs ys] (range xs ys))
        z (partial f 6)]
    (z 12)))


(defn función-partial-3
  []
  (let [f (fn [xs ys] (vector xs ys))
        z (partial f 1)]
    (z 6)))

(defn función-partial-4
  []
  (let [f (fn [xs xy xz] (map xs xy xz))
        z (partial f {0 1} {2 3})]
    (z {1 2})))

(defn función-partial-5
  []
  (let [f (fn [xs ys] (* xs ys))
        z (partial f 90)]
    (z 9)))


(defn función-partial-6
  []
  (let [f (fn [xs ys as bs ]  (list xs ys  as bs))
        z (partial f :a :b)]
    (z :a :b)))

(defn función-partial-7
  []
  (let [f (fn [xs ys] (str xs ys))
        z (partial f "TEST")]
    (z "02:18 AM")))

(defn función-partial-8
  []
  (let [f (fn [xs ys] (mapv xs ys))
        z (partial f dec)]
    (z [ 2 4 6 8 10])))


(defn función-partial-9
  []
  (let [f (fn [xs ys] (map xs ys))
        z (partial f inc)]
    (z [2 4 6 8 10])))

(defn función-partial-10
  []
  (let [f (fn [xs ys] (== xs ys))
        z (partial f 3)]
    (z  3)))

(defn función-partial-11
  []
  (let [f (fn [xs ys] (<= xs ys))
        z (partial f 3)]
    (z  6)))

(defn función-partial-12
  []
  (let [f (fn [xs ys] (>= xs ys))
        z (partial f 9)]
    (z  16)))

(defn función-partial-13
  []
  (let [f (fn [xs ys] (> xs ys))
        z (partial f 29)]
    (z  16)))

(defn función-partial-14
  []
  (let [f (fn [xs ys] (< xs ys))
        z (partial f 2)]
    (z  16)))

(defn función-partial-15
  []
  (let [f (fn [xs ys] (drop-while xs ys))
        z (partial f neg?)]
    (z [-3 -2 -1 0 1 2 3])))


(defn función-partial-16
  []
  (let [f (fn [xs ys] (hash-map xs ys))
        z (partial f 6)]
    (z #{0 1 2 3})))

(defn función-partial-17
  []
  (let [f (fn [xs ys] (drop-last xs ys))
        z (partial f 3)]
    (z [0 1 2 3])))

(defn función-partial-18
  []
  (let [f (fn [xs ys] (- xs ys))
        z (partial f 2)]
    (z  16)))


(defn función-partial-19
  []
  (let [f (fn [xs ys] (+ xs ys))
        z (partial f 2)]
    (z  16)))

(defn función-partial-20
  []
  (let [f (fn [xs ys ] ( / xs ys))
        z (partial f 2)]
    (z  10)))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)


(defn función-some-fn-1
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (number? x))
        h (fn [x] (int? x))
        z (some-fn f g h)]
    (z  1 2 3)))


(defn función-some-fn-2
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (list? x))
        h (fn [x] (map? x))
        z (some-fn f g h)]
    (z  [] '() {})))

(defn función-some-fn-3
  []
  (let [f (fn [xs] (associative? xs))
        g (fn [xs] (list? xs))
        h (fn [xs] (map? xs))
        z (some-fn f g h)]
    (z {:A 1 :B 2 :C 3})))

(defn función-some-fn-4
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (neg? xs))
        h (fn [xs] (int? xs))
        z (some-fn f g h)]
    (z [-2 -1 -1 -2])))

(defn función-some-fn-5
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (zero? xs))
        h (fn [xs] (int? xs))
        z (some-fn f g h)]
    (z [0])))

(defn función-some-fn-6
  []
  (let [f (fn [xs] (number? xs))
        g (fn [xs] (zero? xs))
        h (fn [xs] (int? xs))
        z (some-fn f g h)]
    (z 0)))


(defn función-some-fn-7
  []
  (let [f (fn [xs] (frequencies xs))
        g (fn [xs] (first xs))
        h (fn [xs] (int xs))
        z (some-fn f g h)]
    (z '( 1 2 3 4 5 6 1))))

(defn función-some-fn-8
  []
  (let [f (fn [xs] (frequencies xs))
        g (fn [xs] (last xs))
        h (fn [xs] (string? xs))
        z (some-fn f g h)]
    (z '("a" "e" "i" "o" "u" "a"))))


(defn función-some-fn-9
  []
  (let [f (fn [xs] (peek xs))
        g (fn [xs] (vector? xs))
        h (fn [xs] (string? xs))
        z (some-fn f g h)]
    (z [1 2 3])))

(defn función-some-fn-10
  []
  (let [f (fn [x] (range x))
        g (fn [x] (int? x))
        h (fn [x] (distinct x))
        z (some-fn f g h)]
    (z 23)))

(defn función-some-fn-11
  []
  (let [f (fn [x] (pos-int? x))
        g (fn [x] (int? x))
        h (fn [x] (distinct? x))
        z (some-fn f g h)]
    (z 23)))


(defn función-some-fn-12
  []
  (let [f (fn [xs] (sort xs))
        g (fn [xs] (mapcat xs))
        h (fn [xs] (string? xs))
        z (some-fn f g h)]
    (z [ "1" "2" "3" "4"])))

(defn función-some-fn-13
  []
  (let [f (fn [xs] (concat xs))
        g (fn [xs] (mapcat xs))
        h (fn [xs] (string? xs))
        z (some-fn f g h)]
    (z ["1" "2" "3" "4"])))

(defn función-some-fn-14
  []
  (let [f (fn [xs] (str xs))
        g (fn [xs] (frequencies xs))
        h (fn [xs] (vector? xs))
        z (some-fn f g h)]
    (z ["test" "test"])))

(defn función-some-fn-15
  []
  (let [f (fn [xs] (count xs))
        g (fn [xs] (number? xs))
        h (fn [xs] (even? xs))
        z (some-fn f g h)]
    (z [0 1 2 3 4 5 6])))

(defn función-some-fn-16
  []
  (let [f (fn [xs] (drop-last xs))
        g (fn [xs] (drop-while xs))
        h (fn [xs] (drop xs))
        z (some-fn f g h)]
    (z {:uno 1 :dos 2 :tres 3})))

(defn función-some-fn-17
  []
  (let [f (fn [xs] (flatten xs))
        g (fn [xs] (last xs))
        h (fn [xs] (int xs))
        z (some-fn f g h)]
    (z '( 1 2 3) [ "a" "b"] { 2 5 7 1})))


(defn función-some-fn-18
  []
  (let [f (fn [xs] (rest xs))
        g (fn [xs] (pos? xs))
        h (fn [xs] (vector? xs))
        z (some-fn f g h)]
    (z [1 2 3 4 5])))

(defn función-some-fn-19
  []
  (let [f (fn [xs] (shuffle xs))
        g (fn [xs] (frequencies xs))
        h (fn [xs] (vector? xs))
        z (some-fn f g h)]
    (z [ 1 2 3 4 5])))


(defn función-some-fn-20
  []
  (let [f (fn [xs] (double? xs))
        g (fn [xs] (pos-int? xs))
        h (fn [xs] (list? xs))
        z (some-fn f g h)]
    (z '(1 2 4 6 8))))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)